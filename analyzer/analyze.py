#!/usr/bin/env python3

import argparse

AVAILIABLE_DEVICE_TYPES = ["hdd", "ssd", "nvme"]
AVAILIABLE_LOAD_TYPES = ["read", "randread", "write", "randwrite", "randrw", "rw", "mixed"]
'''
    read - sequentional reads
    randread - random reads
    write - sequentional writes
    randwrite - random writes
    rw - mixed sequential reads and writes
    randrw - mixed random reads and writes
    mixed - mixed sequential and random reads and writes
'''


def main():
    parser = argparse.ArgumentParser(description="Analyze command line interface", add_help=True)
    subpar = parser.add_subparsers(help="Command help",
                                   metavar="{get_recommended_parameters}",
                                   dest="command",
                                   )
    get_recommended_parameters = subpar.add_parser("get_recommended_parameters", help="Get recommended parameters for CAS")
    get_recommended_parameters = get_recommended_parameters.add_argument_group("required arguments")
    get_recommended_parameters.add_argument("-d", "--device-type", dest="device_type", help="The type of RAID drives", required=True,
                                 choices=AVAILIABLE_DEVICE_TYPES)
    get_recommended_parameters.add_argument("-l", "--load-type", dest="load_type",
                                 help="Estimated load type: {read, randread, write, randwrite, randrw, rw, mixed}",
                                 required=True, choices=AVAILIABLE_LOAD_TYPES)
    get_recommended_parameters.add_argument("-bs", "--block-size", dest="block_size", type=int, help="Average estimated block size in KB", required=True)
    get_recommended_parameters.add_argument("-iod", "--iodepth", dest="iodepth", type=int, help="Estimated average number of I/O units", required=True)
    get_recommended_parameters.add_argument("-nj", "--numjobs", dest="numjobs", type=int,
                                 help="Estimated average number of clones (processes/threads performing the same workload)",
                                 required=True)

    try:
        import argcomplete

        argcomplete.autocomplete(parser)
    except ImportError:
        pass

    args = parser.parse_args()
    try:
        if args.command == "get_recommended_parameters":
            if args.load_type == "randread":
                return print("Should not use Open CAS or it is recommended to set Pass-Through (PT) mode")
            if args.load_type == "read":
                if args.device_type == "hdd" and args.block_size >= 64:
                    return print("It is recommended to use Open CAS in Write-Through (WT) mode with cache line size 64 KB")
                else:
                    return print("Should not use Open CAS or it is recommended to set Pass-Through (PT) mode")
            if args.load_type == "randwrite":
                if args.device_type == "ssd" or args.device_type == "nvme":
                    if args.numjobs == 1 and args.iodepth == 1 and args.block_size <= 64:
                        return print("It is recommended to use Open CAS in Write-Only (WO) mode with cache line size 64 KB")
                    else:
                        return print("Should not use Open CAS or it is recommended to set Pass-Through (PT) mode")
                if args.device_type == "hdd":
                    return print("It is recommended to use Open CAS in Write-Only (WO) mode with cache line size 64 KB")
            if args.load_type == "write":
                if args.device_type == "ssd" or args.device_type == "nvme":
                    if args.iodepth == 1 and args.block_size <= 64:
                        return print("It is recommended to use Open CAS in Write-Only (WO) mode with cache line size 64 KB")
                    else:
                        return print("Should not use Open CAS or it is recommended to set Pass-Through (PT) mode")
                if args.device_type == "hdd":
                    return print("It is recommended to use Open CAS in Write-Only (WO) mode with cache line size 64 KB")
            if args.load_type == "randrw":
                if args.device_type == "ssd" or args.device_type == "nvme":
                    if args.numjobs == 1 and args.iodepth == 1 and args.block_size <= 64:
                        return print("It is recommended to use Open CAS in Write-back (WB) mode with cache line size 64 KB")
                    else:
                        return print("Should not use Open CAS or it is recommended to set Pass-Through (PT) mode")
                if args.device_type == "hdd":
                    if args.block_size <= 128:
                        return print("It is recommended to use Open CAS in Write-back (WB) mode with cache line size 64 KB")
                    else:
                        return print("Should not use Open CAS or it is recommended to set Pass-Through (PT) mode")
            if args.load_type == "rw":
                if args.device_type == "ssd":
                    if args.iodepth <= 16 and args.numjobs <= 16:
                        return print("It is recommended to use Open CAS in Write-back (WB) mode with cache line size 64 KB")
                    else:
                        return print("Should not use Open CAS or it is recommended to set Pass-Through (PT) mode")
                if args.device_type == "nvme":
                    if args.iodepth <= 4 and args.numjobs <= 4 and args.block_size <= 64:
                        return print("It is recommended to use Open CAS in Write-back (WB) mode with cache line size 64 KB")
                    else:
                        return print("Should not use Open CAS or it is recommended to set Pass-Through (PT) mode")
                if args.device_type == "hdd":
                    return print("It is recommended to use Open CAS in Write-back (WB) mode with cache line size 64 KB")
            if args.load_type == "mixed":
                if args.device_type == "nvme":
                    if args.iodepth <= 10 and args.numjobs <= 4:
                        return print("It is recommended to use Open CAS in Write-back (WB) mode with cache line size 64 KB")
                    else:
                        return print("Should not use Open CAS or it is recommended to set Pass-Through (PT) mode")
                if args.device_type == "ssd":
                    if args.iodepth <= 20 and args.numjobs <= 8:
                        return print("It is recommended to use Open CAS in Write-back (WB) mode with cache line size 64 KB")
                    else:
                        return print("Should not use Open CAS or it is recommended to set Pass-Through (PT) mode")
                if args.device_type == "hdd":
                    return print("It is recommended to use Open CAS in Write-back (WB) mode with cache line size 64 KB")
            return print("Should not use Open CAS or it is recommended to set Pass-Through (PT) mode")
        elif args.command is None:
            parser.print_help()
    except Exception as e:
        error(str(e))


if __name__ == "__main__":
    main()
