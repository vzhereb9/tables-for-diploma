Запуск: ./analyze.py get_recommended_parameters пар-ры
Необходимые параметры: -d/--device-type - тип дисков, из которых состоит RAID: hdd, ssd, nvme
                       -l/--load-type - тип предполагаемой нагрузки:  read - последовательное чтение, 
                                                                      randread - случайное чтение,   ,                                                                     write - последовательная запись, 
                                                                      randwrite - случайная запись, 
                                                                      rw - смешанные последовательные чтение и запись, 
                                                                      randrw - смешанные случайные чтение и запись, 
                                                                      mixed - случаные и  последовательные чтение и запись.
                        -bs/--block-size - средний ожидаемый размер поступающих блоков в KБ
                        -iod/--iodepth - средняя ожидаемая глубина очереди
                        -nj/--numjobs - среднее ожидаемое количество потоков

Пример запуска: ./analyze.py get_recommended_parameters -d hdd -l randread -bs 32 -iod 8 -nj 2
Пример вывода: Should not use Open CAS or it is recommended to set Pass-Through (PT) mode или It is recommended to use Open CAS in Write-Only (WO) mode with cache line size 64 KB
